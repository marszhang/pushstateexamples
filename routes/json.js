var express = require('express');
var router = express.Router();

/* GET home page. */

router.use('/', function(req, res, next) {
    // ... maybe some additional /bar logging ...
    console.log("this is json api");
    next();
});

// always invoked
router.use(function(req, res, next) {
    console.log(req.baseUrl);
    console.log(req.path);
    //console.log(req);
    if(req.path == '/-/a'){
        res.json({"name":"mars","phone":1592105xxxx});
    }else if(req.path == '/-/a/b'){
        res.json({"name":"mars2","phone":1592105xxxx});
    }else if(req.path == '/-/a/b/c'){
        res.json({"name":"mars3","phone":1592105xxxx});
    }else{
        res.json({"name":"unknown","phone":1592105xxxx});

    }

});

module.exports = router;
