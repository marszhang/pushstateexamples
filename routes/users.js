var express = require('express');
var router = express.Router();

/* GET users listing. */
/*
router.use(function(req, res, next) {
    console.log('%s %s %s', req.method, req.url, req.path);
    next();
});
*/

router.get('/', function(req, res) {
    res.send('respond with a resource');
});

// this will only be invoked if the path starts with /- from the mount point
router.use('/-', function(req, res, next) {
    // ... maybe some additional /bar logging ...
    console.log("this is start with -");
    next();
});

// always invoked
router.use(function(req, res, next) {
    console.log(req.baseUrl);
    console.log(req.path);
    res.render('users',{query:req.path});
});

module.exports = router;
